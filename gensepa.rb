#!/usr/bin/env ruby

require_relative "../bundle/bundler/setup"
require "csv"
require "sepa_king"
require "date"
require "yaml"

def middle_of_month(input)
	if input.nil?
		return Date.today - Date.today.mday + 15
	else
		return Date.strptime(input, "%Y-%m") + 14
	end
end

$date = middle_of_month(ARGV[0])
$date_string = $date.strftime("%Y-%m")
$config = YAML.load_file('config.yml')
$message = "#{$config["statement"]}/#{$date_string}"
$output_file = "output/#{$config["statement"]}_#{$date_string}.xml"
$sum_members = 0
$sum_transaction = 0
$error = false

$input_file = $config["memberlist"]

puts $input_file
$sdd = SEPA::DirectDebit.new($config["banking"])
$sdd.message_identification = $message

def comma_number_to_f(input)
	if input.nil?
		return 0
	else
		tmp = input.gsub(/\D/, '') # remove everything non digit
		return tmp.to_f/100
	end
end

def generate_sepa(user)
	begin
		$fee = comma_number_to_f(user[:beitrag])
		$entry_date = Date.strptime( user[:mitgliedschaftsbeginn], "%d.%m.%Y")
		$exit_date = user[:mitgliedschaftsende] ? Date.strptime( user[:mitgliedschaftsende], "%d.%m.%Y") : false

		return if ($fee == 0)
		return if (user[:iban].nil?)
		return if (user[:abbuchungssperre] != "nein")
		return if (user[:beitragsintervall] != "monatlich")
		return if ($entry_date > $date)
		return if $exit_date && ($exit_date < $date)

		$sum_members += 1

		$sum_transaction += $fee

		$sdd.add_transaction(
			name: (user[:vorname] || "") + " " + user[:nachnamefirma],

			bic: user[:bic],

			iban: user[:iban],

			amount: $fee,

			remittance_information: $message,

			mandate_id: user[:nr],

			mandate_date_of_signature: $entry_date,

			#   'CORE' ("Basis-Lastschrift")
			#   'COR1' ("Basis-Lastschrift mit verkürzter Vorlagefrist")
			#   'B2B' ("Firmen-Lastschrift")
			local_instrument: "CORE",

			#   'FRST' ("Erst-Lastschrift")
			#   'RCUR' ("Folge-Lastschrift")
			#   'OOFF' ("Einmalige Lastschrift")
			#   'FNAL' ("Letztmalige Lastschrift")
			sequence_type: "FRST",

			requested_date: Date.today + 10,

			batch_booking: false
		)

	rescue Exception=>e
		puts "Member #{user[:nr]}: #{e}"
		$error = true
	end
end

CSV.foreach($input_file, :col_sep => ';', :quote_char=>'"', :headers => true, :header_converters => :symbol, :converters => :all, :encoding => 'UTF-8') do |row|
	user = row.to_hash
	generate_sepa(user)
end

if( $sum_transaction <= 0 )
	puts "The sum of all transactions is zero!"
	$error = true
end

if (!$error)
	puts "Members: #{$sum_members}"
	puts "Sum: #{$sum_transaction}"

	File.write($output_file, $sdd.to_xml)
end
