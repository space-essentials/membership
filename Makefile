IMAGE = registry.gitlab.com/space-essentials/membership

.PHONY: image
image:
	docker build -t ${IMAGE} .

.PHONY: shell
shell:
	docker run -it --rm -v $$PWD:/app ${IMAGE} bash

.PHONY: test
test: image
	cd example && make
