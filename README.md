# Membership

CSV based member database with a docker packaged cli tool to generate monthly SEPA debit files.

## Usage

* install dependencies `apt install make docker`
* see [example folder](example) or download [last release](https://gitlab.com/space-essentials/membership/-/jobs/artifacts/master/download?job=dist)
* enter your bank details in `config.yml`
* fill the member list in `members.csv`
* run `make` (for a specific month `make month=2018-01`)
* upload the resulting sepa xml in the output dir to your bank

## Development

* run `make test`
* [iban examples](http://ibanvalidieren.de/beispiele.html)
