FROM ruby:2.5 as build
COPY Gemfile .
RUN bundle install --standalone --clean

FROM ruby:2.5-slim
COPY --from="build" /usr/local/bundle /usr/local/bundle
COPY gensepa.rb /usr/local/bin/gensepa
RUN chmod +x /usr/local/bin/gensepa
WORKDIR /app
CMD ["/usr/local/bin/gensepa"]
